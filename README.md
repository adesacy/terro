---
title: Projet Terro - Nettoyage des données
tags: DataLab, projet Terro, DLWeb
date: 31/03/2021
author: Antoine Silvestre de Sacy
---

# Projet Terro - Nettoyage des données.

## Données

Les dossier V1, V2 contiennent les fichiers contenant la requête Solr correspondante à l'extraction (en version simplifiée et complète) :

- V1 -> collections:actualités AND crawl_date:[2020-09-01T00:00:00Z TO 2020-09-30T23:59:59Z]  AND text:(procès AND attentats AND (charlie OR 2015)) AND url_type:(normal OR slashpage).

- V2 -> collections:actualités AND crawl_date:[2020-09-01T00:00:00Z TO 2020-09-30T23:59:59Z] AND text:("procès" AND "Charlie Hebdo" AND "janvier 2015" AND "attentats")  AND title:("janvier 2015" OR "Hyper Cacher" OR "Charlie Hebdo" OR "procès" OR "attentats") AND url_type:normal.

Le dossier CDX contient les index des URLs extraites pour l'actualité et la presse payante. 

## Étapes suivis lors des traitements des CDX : 

- 717 Urls (actualités + presse payante).

### Suppression des doublons éventuels dans les données.

Aucun doublon recensé dans le corpus.

### Dénombrement du nombre de DNS dans le corpus : 

- Application d'une expression régulière pour extraction des URLs et retrait de tout ce qui fait partie des sous-domaines. On ne veut garder que le corps de l'URL : ex : 
	- https://actu.orange.fr/france/attentats-de-janvier-2015-a-la-veille-du-proces-charlie-hebdo-republie-les-caricatures-de-mahomet-magic-CNT000001sTGqk.amp ==> https://actu.orange.fr
	- https://amp.lesinrocks.com/2020/09/02/actualite/societe/attentats-de-janvier-2015-voici-les-autres-proces-qui-ont-ete-filmes/ ==> https://amp.lesinrocks.com
- Dénombrement des DNS : 


![](https://codimd.s3.shivering-isles.com/demo/uploads/944f58d4edc3d9793ff685d35.png)

- Export : **corpus_count_DNS.csv**.


### Analyse des contenus dans les DNS en retirant les domaines de premier niveau : 

Cette fois-ci, nous souhaitons conserver seulement la partie signifiante des URLs : ex : 
- https://actu.orange.fr/france/attentats-de-janvier-2015-a-la-veille-du-proces-charlie-hebdo-republie-les-caricatures-de-mahomet-magic-CNT000001sTGqk.amp ==> attentats de janvier 2015 a la veille du proces charlie hebdo republie les caricatures de mahomet.

Pour cela : 
- Retrait des domaines de premier niveau : expression régulière : "\\.\\w+$".
- Extraction des noms d'articles (dernière partie de l'URL) : expression régulière : "\\/\\w+-\\w+-\\w+-.*".
- Nettotage du slash.
- Retrait des rangs = "character(0)".
- Dédoublonnage des URLs identiques après tous ces nettoyages : 309 retraits.
- Remplacement des -, _, +, par des espaces.

Voilà à quoi ressemblent maintenant les URLS : 

|    	| URL_clean                                                                                                                                                                                                                                                                                                              	|
|----	|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|
| 1  	| attentats de janvier 2015 a la veille du proces charlie hebdo republie les caricatures de mahomet magic                                                                                                                                                                                   	|
| 2  	| sur un mur parisien les victimes de l attentat de charlie hebdo peintes par l artiste christian guemy alias c215 photographiees le 31 aout 2020                                                	|
| 3  	| cinq ans apres charlie le proces des attentats de janvier 2015 s ouvre a paris                                                                                                                                                                                                                         	|
| 4  	| cinq ans apres charlie le proces des attentats de janvier 2015 s ouvre a paris                                                                                                                                                                                                                           	|
| 5  	| le proces des attentats de janvier 2015 a l heure de l attaque contre l hyper cacher                                                                                                                                                                                                                     	|
| 6  	| photos de yoav hattab yohan cohen philippe braham et francois michel saada tues dans l attaque de l hyper cacher en janvier 2015 accrochees devant l epicerie en juin 2015  	|
| 7  	| la couverture de l hebdomadaire charlie hebdo le 1er septembre 2020 edition publiee la veille de l ouverture du proces de l attentat                                                      	|
| 8  	| ouverture sous tension du proces historique des attentats de janvier 2015                                                                                                                                                                                                                                	|
| 9  	| proces des attentats de janvier 2015 qui sont les 14 accuses magic                                                                                                                                                                                                                                       	|
| 10 	| proces des attentats de janvier 2015 l emotion de la mere de clarissa jean philippe                                                                                                                                                                                                                      	|



- Tokénisation (la "tokénisation" du corpus permet d'isoler ou de "découper" chaque mot présent dans les URLs nettoyées que nous avons extraites afin de pouvoir ensuite analyser leur fréquence d'apparition, de voir quels mots apparaissent les uns avec les autres, etc.).
- Implémentation d'une liste de mots outils que nous souhaitons retirer car ils ne sont pas signifiants et représentent la majeur partie des fréquences (ex : "de", "le", "la", etc.).



### Mots les plus fréquents sur l'ensemble des URLs signifiantes : 

Nous cherchons ensuite à voir sur l'ensemble des URLs signifiantes que nous avons extraites les mots qui ressortent le plus fréquemment : 

![](https://codimd.s3.shivering-isles.com/demo/uploads/944f58d4edc3d9793ff685d37.png)



### Sur les six premiers journaux : 

Pourrait-on essayer de voir, journaux par journaux, quels sont les mots qui sont spécifiques de leurs URLS ? 

- Tokénisation du corpus en gardant le journal d'origine.
- Ajout de la fréquence relative pour normaliser le nombre de mots par journal.
- Sélection des six premiers DNS du corpus.
- Ajout de stopwords.

![](https://codimd.s3.shivering-isles.com/demo/uploads/944f58d4edc3d9793ff685d36.png)

### Analyse en bigrams :

Nous avons déjà pu voir quels étaient les mots qui apparaissaient le plus fréquemment dans le corpus nettoyé d'URLs ; cependant, il serait intéressant de voir les mots qui sont le plus souvent associés au sein même de ces URLs, c'est-à-dire les mots qui sont les plus fréquemment employés de façon conjointe dans une même URL. Après avoir récupérer les mots tokénisés et appliqués les mêmes nettoyages que précedemment sur chaque URL correspondant à un nom de domaine, nous obtenons la liste de fréquence suivante (top 25, les résultats complets sont disponibles dans le fichier corpus_count_ngram.csv) :

### Visualisation sous la forme d'un réseau : 

![](https://codimd.s3.shivering-isles.com/demo/uploads/944f58d4edc3d9793ff685d3b.png)


## Étapes suivies lors du traitement des requêtes SolR : 

### Requêtes SolR 

- Lecture de la V2 -> collections:actualités AND crawl_date:[2020-09-01T00:00:00Z TO 2020-09-30T23:59:59Z] AND text:("procès" AND "Charlie Hebdo" AND "janvier 2015" AND "attentats")  AND title:("janvier 2015" OR "Hyper Cacher" OR "Charlie Hebdo" OR "procès" OR "attentats") AND url_type:normal.
- Fichier : V13.txt
- 54 322 Urls : domain, title, url, wayback_date

### Nettoyages : 


- Homogénéisation des dates et des heures en deux colonnes distinctes : 20-09-2020 08:44:11 / 20-09-2020 / 08:44:11
- Retrait des doublons : 32 790 retraits, reste 21 532 observations.
- Dénombrement des DNS : 75 DNS distincts, ie 75 sites différents dans le corpus dédoublonné.

**Visualisation :**

![](https://codimd.s3.shivering-isles.com/demo/uploads/944f58d4edc3d9793ff685d43.png)


- En pourcentage, les dix premiers sites qui apparaissent le plus souvent représentent 74.7% du corpus.

| ID   	| domain                  	| n    	| percentage 	|
|----	|-------------------------	|------	|------------	|
| 1  	| ouest-france.fr         	| 4912 	| 22.8       	|
| 2  	| 20minutes.fr            	| 2364 	| 11         	|
| 3  	| liberation.fr           	| 1723 	| 8          	|
| 4  	| la-croix.com            	| 1377 	| 6.4        	|
| 5  	| ladepeche.fr            	| 1306 	| 6.1        	|
| 6  	| humanite.fr             	| 1189 	| 5.5        	|
| 7  	| orange.fr               	| 1165 	| 5.4        	|
| 8  	| atlantico.fr            	| 851  	| 4          	|
| 9  	| lepoint.fr              	| 668  	| 3.1        	|
| 10 	| nrpyrenees.fr           	| 509  	| 2.4        	|

![](https://codimd.s3.shivering-isles.com/demo/uploads/944f58d4edc3d9793ff685d45.png)


- Export dans le fichier **DNS_count_SolR_V2.csv**.


## Focus sur l'analyse des titres : 

- Dédoublonnage des titres identiques : 5376 retraits.
- **Export** : titres_dedoublonnes.csv.
- Tokénisation du corpus en gardant le journal d'origine.
- Ajout de la fréquence relative.
- Sélection des six premiers DNS : 
    - ouest-france.fr
    - 20minutes.fr 
    - liberation.fr
    - la-croix.com
    - ladepeche.fr
    - humanite.fr
- Ajout de stopwords.
- Visualisation des mots les plus fréquents dans les titres journal par journal en fréquences relatives : 

![](https://codimd.s3.shivering-isles.com/demo/uploads/944f58d4edc3d9793ff685d48.png)

- Visualisation des mots les plus fréquents dans les titres journal par journal en fréquences absolues : 

![](https://codimd.s3.shivering-isles.com/demo/uploads/944f58d4edc3d9793ff685d4b.png)


## Analyse des réseaux lexicaux (bigrams) : 

Il serait intéressant de voir les mots qui sont le plus souvent associés au sein de ces titres, c'est-à-dire les mots qui sont les plus fréquemment employés de façon conjointe dans un même titre. Après avoir récupérer les mots tokénisés et appliqués les mêmes nettoyages que précedemment, nous obtenons la liste de fréquence suivante : 

|  Id  	| bigrams                	| n  	|
|----	|------------------------	|----	|
| 1  	| 2015 attentats         	| 14 	|
| 2  	| blanche blessés        	| 11 	|
| 3  	| terroriste terroristes 	| 10 	|
| 4  	| charlie hebdo          	| 8  	|
| 5  	| charlie procès         	| 8  	|
| 6  	| menacé menaces         	| 8  	|
| 7  	| ans après              	| 7  	|
| 8  	| attentats janvier      	| 7  	|
| 9  	| charlie janvier        	| 7  	|
| 10 	| paris pas              	| 7  	|
| ... 	| ...              	| ...  	|


### Visualisation des bigrams sous la forme d'un réseau :

![](https://codimd.s3.shivering-isles.com/demo/uploads/944f58d4edc3d9793ff685d4c.png)

### Visualisation d'un mot spéficique et de ses mots liés :

Nous souhaitons également pouvoir être en mesure de faire un focus sur un mot particulier. Essayons, par exemple, de voir le réseau lexical du mot "charlie" et à quoi celui-ci est le plus souvent lié dans les titres associé à l'url de la requête SolR : 

#### Le mot "charlie" : 

![](https://codimd.s3.shivering-isles.com/demo/uploads/944f58d4edc3d9793ff685d4d.png)

#### Le mot "procès" : 

![](https://codimd.s3.shivering-isles.com/demo/uploads/944f58d4edc3d9793ff685d4e.png)









